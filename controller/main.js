// import { Person, Student } from "./modal";

var List= new ListPerson();
let person =new Person();
callModal= (modal_title,readOnly,type)=>{
    document.getElementById("modal-title").innerHTML=modal_title;
    document.getElementById("id").readOnly=readOnly;

    switch (type){
        case 1:
            document.getElementById("btn-add").style.display="block";
            document.getElementById("btn-update").style.display="none";
            break;
        case 2:
            document.getElementById("btn-add").style.display="none";
            document.getElementById("btn-update").style.display="block";
            break;
            
    }
}
xoaForm=()=>{
    let elements= document.getElementsByClassName("form-control");
    for(let element of elements){
        element.value="";
    }
    document.getElementById("type").selectedIndex=0;
}

renderList=(dsnv)=>{
    let contentHtml="";
    for (var i = 0; i < dsnv.length; i++) {
        // duyệt mảng dssv từ đầu tới cuối
        var sv = dsnv[i];
      let content=`<tr>
      <td>${sv.ma}</td>
      <td>${sv.hoTen}</td>
      <td>${sv.diaChi}</td>
      <td>${sv.email}</td>
      <td style="color: red" >${sv.mangDoiChieu}</td>
      <td>${sv.tenCongTy}</td>
      <td>${sv.giaHoaDon}</td>
      <td>${sv.danhGia}</td>
      <td>${sv.caculateSalary}</td>
      <td>
      <button id="update" onclick="updatePerson('${sv.ma}')" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap" class="btn btn-warning">sửa</button> 
      <button onclick="deletePerson('${sv.ma}')" class="btn btn-success">xóa</button> <td>
      </tr>`
      contentHtml+=content;
 
    }
    document.getElementById("contentList").innerHTML=contentHtml
}

document.getElementById("btn-them").addEventListener("click",()=>{
    xoaForm();
    callModal("THÊM NGƯỜI DÙNG",false,1);
})
document.getElementById("btn-add").addEventListener("click",()=>{
    let type=document.getElementById("type").value;
    let name=document.getElementById("name").value;
    let id=document.getElementById("id").value;
    let address= document.getElementById("address").value;
    let email=document.getElementById("email").value;
    let toan=document.getElementById("Math").value*1;
    let ly=document.getElementById("Physic").value*1;
    let hoa=document.getElementById("Chemistry").value*1;
    let companyName=document.getElementById("company_name").value;
    let bill=document.getElementById("bill").value;
    let evaluate=document.getElementById("evaluate").value;
    let working=document.getElementById("working").value;
    let salary=document.getElementById("salary").value;
    if(type=="Student"){
     let newStudent= new Student(name,id,address,email,toan,ly,hoa);
    List.addPerson(newStudent);
      renderList(List.ListPersons);
    }else if(type=="Customer"){
        let newCustomer= new Customer(name,id,address,email,companyName,bill,evaluate);
        List.addPerson(newCustomer);
        renderList(List.ListPersons);
    }else if(type==="Employee"){
      let newEmployee= new Employee(name,id,address,email,working,salary);
      List.addPerson(newEmployee);
      renderList(List.ListPersons);
    }
})

document.getElementById("company-input").style.display="none";
document.getElementById("bill-input").style.display="none";
document.getElementById("evaluate-input").style.display="none";
document.getElementById("math-input").style.display="none";
document.getElementById("Physic-input").style.display="none";
document.getElementById("Chemistry-input").style.display="none";
document.getElementById("working-input").style.display="none";
document.getElementById("salary-input").style.display="none";
 handleTypeChange=()=> {
    var typeSelect = document.getElementById("type");
    var selectedValue = typeSelect.value;
    if (selectedValue === "Student") {
        //Student
      document.getElementById("math-input").style.display="block";
      document.getElementById("Physic-input").style.display="block";
      document.getElementById("Chemistry-input").style.display="block";
      
      document.getElementById("working-input").style.display="none";
      document.getElementById("salary-input").style.display="none";
      document.getElementById("company-input").style.display="none";
      document.getElementById("bill-input").style.display="none";
      document.getElementById("evaluate-input").style.display="none";
    } else if (selectedValue === "Customer") {
        document.getElementById("math-input").style.display="none";
      document.getElementById("Physic-input").style.display="none";
      document.getElementById("Chemistry-input").style.display="none";
      document.getElementById("working-input").style.display="none";
      document.getElementById("salary-input").style.display="none";
      //  Customer
      document.getElementById("company-input").style.display="block";
      document.getElementById("bill-input").style.display="block";
      document.getElementById("evaluate-input").style.display="block";

    } else if (selectedValue === "Employee") {
      document.getElementById("company-input").style.display="none";
      document.getElementById("bill-input").style.display="none";
      document.getElementById("evaluate-input").style.display="none";
      document.getElementById("math-input").style.display="none";
      document.getElementById("Physic-input").style.display="none";
      document.getElementById("Chemistry-input").style.display="none";
      document.getElementById("working-input").style.display="none";
      document.getElementById("salary-input").style.display="none";
      // Employee
      document.getElementById("working-input").style.display="block";
      document.getElementById("salary-input").style.display="block";
     
    }
  }




document.getElementById("btn-update").addEventListener("click",()=>{
    let name=document.getElementById("name").value;
    let id=document.getElementById("id").value;
    let address= document.getElementById("address").value;
    let email=document.getElementById("email").value;
    let newPerson= new Person(name,id,address,email);
    List.updatePerson(newPerson);
    renderList(List.ListPersons);
  }
  )

deletePerson=(ma)=>{
  console.log(ma);
  List.deletePerson(ma);
  renderList(List.ListPersons);
}
updatePerson=()=>{
  callModal("update",true,2);
  let type=document.getElementById("type").value;
  let name=document.getElementById("name").value;
  let id=document.getElementById("id").value;
  let address= document.getElementById("address").value;
  let email=document.getElementById("email").value;
  let toan=document.getElementById("Math").value*1;
  let ly=document.getElementById("Physic").value*1;
  let hoa=document.getElementById("Chemistry").value*1;
  let companyName=document.getElementById("company_name").value;
  let bill=document.getElementById("bill").value;
  let evaluate=document.getElementById("evaluate").value;
 
  if(type=="Student"){
    let newStudent= new Student(name,id,address,email,toan,ly,hoa);
   List.updatePerson(newStudent);
     renderList(List.ListPersons);
   }else if(type=="Customer"){
       let newCustomer= new Customer(name,id,address,email,companyName,bill,evaluate);
       List.updatePerson(newCustomer);
       renderList(List.ListPersons);
   }else if(type==="Employee"){
    let newEmployee= new Employee(name,id,address,email,working,salary);
    List.updatePerson(newEmployee);
    renderList(List.ListPersons);
  }
}
sapXepTang= ()=>{
    List.SapXepNhanVien(1);
  renderList(List.ListPersons);
  }
document.getElementById("sapXepTang").addEventListener("click", ()=>{

  List.SapXepNhanVien(-1);
  renderList(List.ListPersons);
  })


